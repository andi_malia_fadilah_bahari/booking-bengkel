package com.bengkel.booking.services;

import java.util.List;
import java.util.stream.Collectors;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.println();
		printHeader(title, line.length(), line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		if (listMenu.length > 0) {
			System.out.println(line);
			System.out.println();
		}
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+";
		System.out.println(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.println(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.println(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.

	public static void printHeader (String contextHeader, int lineLength, String lineBorder) {
		lineLength -= 4;
		int padding1 = (lineLength + contextHeader.length()) / 2;
		int padding2 = lineLength - padding1;

		System.out.println(lineBorder);
		System.out.format("| %"+padding1+"s%-"+padding2+"s |%n", contextHeader,"");
		System.out.println(lineBorder);
	}
	
	public static void printCustomer(Customer customer) {
		boolean isMember = false;
		String line = "+----------------------+--------------------------------------------------------------------------+";
		String formatTable = "| %-20s | %-72s |%n";
		String[][] listColumn = {
			{"", "Customer Id", "Nama", "Customer Status", "Alamat", "Saldo Koin", "List Kendaraan"},
			{"", customer.getCustomerId(), customer.getName(), "", customer.getAddress(), "", ""}
		};

		System.out.println(line);
		for (int i = 0; i < listColumn[0].length; i++) {
			if (i == 0) {
				listColumn[0][0] = (customer instanceof MemberCustomer)?"Member":"Non Member";
				listColumn[1][3] = listColumn[0][0];
				if (listColumn[0][0].equalsIgnoreCase("Member")) {
					listColumn[1][5] = Double.toString(((MemberCustomer)customer).getSaldoCoin());
					isMember = true;
				}
			}
			if (i == 5) {
				if (isMember) {
					System.out.format(formatTable, listColumn[0][i],listColumn[1][i]);
				}
			}
			else {
				System.out.format(formatTable, listColumn[0][i],listColumn[1][i]);
			}
			if (i == 0) {
				printHeader("Customer Profile", line.length(), line);
			}
		}
		printVechicle(customer.getVehicles());
	}
	
	public static void printService (List<ItemService> vehicleServices) {
		String line = "+----+-----------------+----------------------+-----------------+-----------------+";
		String formatTable = "| %-2s | %-15s | %-20s | %-15s | %-15s |%n";
		String[] listHeader = {"No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga"};

		printHeader("List Service Yang Tersedia", line.length(), line);
		System.out.format(formatTable, listHeader);
		int index = 1;
		for (ItemService service : vehicleServices) {
			System.out.format(formatTable, index, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
			index++;
		}
		System.out.println(line);
	}

	public static void printBooking (List<BookingOrder> listBookingOrders) {
		String formatTable = "| %-2s | %-20s | %-20s | %-15s | %-15s | %-15s | %-32s |%n";
		String line = "+----+----------------------+----------------------+-----------------+-----------------+-----------------+----------------------------------+";
		System.out.println(line);
	    System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service");
	    System.out.println(line);
	    int number = 1;
	    String vehicleType = "";
	    listBookingOrders
		.stream()
		.forEach(booking -> {
			System.out.format(formatTable,
				number,
				booking.getBookingId(),
				booking.getCustomer().getName(),
				booking.getPaymentMethod(),
				booking.getTotalServicePrice(),
				booking.getTotalPayment(),
				booking.getServices()
				.stream()
				.map(service -> service.getServiceName())
				.collect(Collectors.toList())
			);
		});
		System.out.println(line);
	    System.out.format("| %-2s | %-"+(line.length() - 9)+"s |%n", "0","Exit / Kembali");
	    System.out.println(line);
	}
}
