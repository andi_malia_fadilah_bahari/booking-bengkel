package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Motorcyle;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	
	//Info Customer
	public static Customer getCustomer (List<Customer> listAllCustomers, String costumerId) {
		Customer customer = listAllCustomers
		.stream()
		.filter(cust -> cust.getCustomerId().equalsIgnoreCase(costumerId))
		.findAny()
		.orElse(null);

		return customer;
	}
	//Booking atau Reservation
	public static BookingOrder bookingMenu (Customer customer, List<ItemService> allItemServices, List<Vehicle> allCustomerVehicle) {
		BookingOrder order = null;
		String vehicleId;
		Vehicle vehicle = null;
		List<ItemService> listSelectedService = new ArrayList<>();
		List<ItemService> UsedlistService = new ArrayList<>();
		List<ItemService> listServiceMotor = allItemServices
		.stream()
		.filter(service -> service.getVehicleType().equalsIgnoreCase("Motorcyle"))
		.collect(Collectors.toList());

		List<ItemService> listServiceCar = allItemServices
		.stream()
		.filter(service -> service.getVehicleType().equalsIgnoreCase("Car"))
		.collect(Collectors.toList());
		
		PrintService.printMenu(new String[0], "Booking Service");
		do {
			vehicleId = Validation.validasiInput("Masukan Vehicle ID: ", "Input Tidak Boleh Mengandung Spasi", "^[^ %n]+$");
			vehicle = getVehicle(customer.getVehicles(), vehicleId);

			if (vehicle == null) {
				System.out.println("Notifikasi Pesan bahwa Kendaraan Tidak ditemukan.");
			}
		} while (vehicle == null);
		if (vehicle instanceof Motorcyle) { UsedlistService = listServiceMotor; }
		else if (vehicle instanceof Car) { UsedlistService = listServiceCar; }


		//Pemilihan Jenis Servis
		String serviceLoop = "Y";
		String serviceId;
		ItemService selectedService = null;
		ItemService selectedServiceFromAll = null;
		int totalService = 0;
		do {
			do {
				PrintService.printService(UsedlistService);
				serviceId = Validation.validasiInput("Silahkan masukan Service Id:", "Input Tidak Boleh Mengandung Spasi", "^[^ %n]+$");
				selectedService = getItemService(UsedlistService, serviceId);
				selectedServiceFromAll = getItemService(allItemServices, serviceId);

				if (selectedService == null) {
					if (selectedServiceFromAll == null) { System.out.println("Service Id Tidak Ditemukan atau Salah!"); }
					else { System.out.println("Service Id Sudah Dipesan"); }
				}
			} while (selectedService == null);
			UsedlistService.remove(selectedService);
			listSelectedService.add(selectedService);

			totalService++;
			serviceLoop = Validation.validasiInput("Apakah anda ingin menambahkan Service Lainnya? (Y/T)", "Input Hanya Boleh Mengandung Y/T", "^[tyTY]+$");
			if ((totalService >= customer.getMaxNumberOfService())) {
				System.out.println("Anda Sudah Memenuhi Batas Service Anda");
			}
		} while ((serviceLoop.equalsIgnoreCase("Y")) && (totalService < customer.getMaxNumberOfService()));


		//Pembayaran
		String[] paymentMethodAvaiable = {
			"Saldo Coin", "Cash"
		};
		String paymentMethod = paymentMethodAvaiable[0];
		boolean paymentLoop = true;
		order = new BookingOrder(customer, listSelectedService, paymentMethod);
		do {
			paymentMethod = Validation.validasiInput("Silahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)", "Input Hanya Boleh Alfabet dan Spasi", "^[a-zA-Z ]+$");
			for (String method : paymentMethodAvaiable) {
				if (paymentMethod.equalsIgnoreCase(method)) {
					if (paymentMethod.equalsIgnoreCase(paymentMethodAvaiable[0])) {
						if (!(customer instanceof MemberCustomer)) {
							System.out.println("HANYA Customer MEMBERSHIP Yang Bisa Menggunakan Metode Pembayaran Ini");
						}
						else if (((MemberCustomer)customer).getSaldoCoin() < order.getTotalPayment()) {
							System.out.println("Saldo Coin Anda Kurang");
						}
						else {
							order.setPaymentMethod(method);
							order.calculatePayment();
							paymentLoop = false;
							break;
						}
					}
					else {
						order.setPaymentMethod(method);
						order.calculatePayment();
						paymentLoop = false;
						break;
					}
				}
			}
		} while (paymentLoop);

		return order;
	}
	public static List<Vehicle> getAllCustomerVehicle (List<Customer> listAllCustomers) {
		return listAllCustomers
		.stream()
		.flatMap(customer -> customer.getVehicles().stream())
		.collect(Collectors.toList());
	}
	public static Vehicle getVehicle (List<Vehicle> allCustomerVehicle, String vehicleID) {
		return allCustomerVehicle
		.stream()
		.filter(vehicle -> vehicle.getVehiclesId().equalsIgnoreCase(vehicleID))
		.findAny()
		.orElse(null);
	}
	public static ItemService getItemService (List<ItemService> allItemServices, String serviceId) {
		return allItemServices
		.stream()
		.filter(service -> service.getServiceId().equalsIgnoreCase(serviceId))
		.findAny()
		.orElse(null);
	}

	public static List<BookingOrder> getCustomerBooking (Customer customer, List<BookingOrder> allBookingOrders) {
		return allBookingOrders
		.stream()
		.filter(booking -> booking.getCustomer().equals(customer))
		.collect(Collectors.toList());
	}

	//Top Up Saldo Coin Untuk Member Customer 
	public static double topUpSaldoCoin (Customer customer) {
		double saldoCoin = 0;
		PrintService.printMenu(new String[0], "Top Up Saldo Coin");
		System.out.format("%-19s:%s%n", "Saldo Coin Saat ini", ((MemberCustomer)customer).getSaldoCoin());
		saldoCoin = Double.parseDouble(Validation.validasiInput("Masukan besaran Top Up:", "Input Harus Berupa Angka!", "^[0-9]+$"));
		return saldoCoin;
	}
	//Logout
	
}
