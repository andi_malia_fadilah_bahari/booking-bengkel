package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<Vehicle> listAllCostumerVehicle = BengkelService.getAllCustomerVehicle (listAllCustomers);
	private static Scanner input = new Scanner(System.in);

	private static Customer customers = null;
	private static List<BookingOrder> listAllBookingOrders = new ArrayList<>();

	public static void run() {
		boolean isLooping = true;
		do {
			String[] listMenu = {"Login", "Exit/Keluar"};
			int menuChoice = 0;
			PrintService.printMenu(listMenu, "Aplikasi Booking Bengkel");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			
			switch (menuChoice) {
				case 1:
					login();
					mainMenu();
					break;
				case 0:
					isLooping = false;
			}
		} while (isLooping);
		
	}
	
	public static void login() {
		int errorInput = -1;
		do {
			errorInput++;
			if (errorInput >= 3) {
				System.out.println("Terjadi Kesalahan Dalam Login Selama 3x Silahkan Mulai Ulang Aplikasi");
				System.exit(0);
			}
			
			PrintService.printMenu(new String[0], "Login Aplikasi");
			String customerId = Validation.validasiInput("Masukan Customer ID: ", "Input Tidak Boleh Mengandung Spasi", "^[^ %n]+$");
			
			customers = BengkelService.getCustomer(listAllCustomers, customerId);
			if (customers == null) {
				System.out.println("Customer Id Tidak Ditemukan atau Salah!");
				continue;
			}
			
			String customerPass = Validation.validasiInput("Masukan Password: ", "Input Tidak Boleh Mengandung Spasi", "^[^ %n]+$");
			if (!customers.getPassword().equalsIgnoreCase(customerPass)) {
				System.out.println("Password yang anda Masukan Salah!");
				customers = null;
			}
		} while (customers == null);
		System.out.println("Selamat Datang "+customers.getName());
	}
	
	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		int indexCustomer = listAllCustomers.indexOf(customers);
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			// System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				PrintService.printCustomer(customers);
				break;
			case 2:
				//panggil fitur Booking Bengkel
				listAllBookingOrders.add(BengkelService.bookingMenu(customers, listAllItemService, listAllCostumerVehicle));
				
				System.out.println();
				System.out.println("Booking Berhasil");
				System.out.println();
				
				BookingOrder lastBookingOrder = listAllBookingOrders.get(listAllBookingOrders.size()-1);
				double totalServicePrice = lastBookingOrder.getTotalServicePrice();
				double totalPayment = lastBookingOrder.getTotalPayment();
				String formatTable = "%-20s: %20s%n";
				String[][] priceTable = {
					{"Total Harga Service", "Total Pembayaran"},
					{Double.toString(totalServicePrice), Double.toString(totalPayment)}
				};
				for (int i = 0; i < priceTable[0].length; i++) {
					System.out.format(formatTable, priceTable[0][i], priceTable[1][i]);
				}


				if (listAllCustomers.get(indexCustomer) instanceof MemberCustomer && lastBookingOrder.getPaymentMethod().equalsIgnoreCase("Saldo Coin")) {
					((MemberCustomer)listAllCustomers.get(indexCustomer))
					.setSaldoCoin(((MemberCustomer)customers).getSaldoCoin() - totalPayment);
				}
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				if (listAllCustomers.get(indexCustomer) instanceof MemberCustomer) {
					double topUp = BengkelService.topUpSaldoCoin(customers);
					((MemberCustomer)listAllCustomers.get(indexCustomer))
					.setSaldoCoin(((MemberCustomer)customers).getSaldoCoin() + topUp);
					System.out.println("Top Up Berhasil Dilakukan");
				}
				else {
					System.out.println("HANYA Customer MEMBERSHIP Yang Bisa Menggunakan Fitur Ini");
				}
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				String loopingBookView = "1";
				do {
					List<BookingOrder> listCustomerBookingOrders = BengkelService.getCustomerBooking(customers, listAllBookingOrders);
					PrintService.printBooking(listCustomerBookingOrders);
					loopingBookView = Validation.validasiInput("Tekan (0) Untuk Kembali Ke Menu Utama", "Input Hanya Boleh Angka (0)", "^[0]+$");
				} while (!loopingBookView.equalsIgnoreCase("0"));
				break;
			default:
				System.out.println("Logout");
				isLooping = false;
				customers = null;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
