package com.bengkel.booking.models;

import java.util.List;
import com.bengkel.booking.interfaces.IBengkelPayment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingOrder implements IBengkelPayment{
	private String bookingId;
	private Customer customer;
	private List<ItemService> services;
	private String paymentMethod;
	private double totalServicePrice;
	private double totalPayment;
	private static int idCount = 1;

    public BookingOrder(Customer customer, List<ItemService> services, String paymentMethod) {
        this.customer = customer;
        this.services = services;
        this.paymentMethod = paymentMethod;

		String customerIDString = getCustomer().getCustomerId();
		String[] customerIDArray = customerIDString.split("-", 0);
		setBookingId("Book-" + customerIDArray[0] + "-00" + idCount +"-"+ customerIDArray[1]);
		calculateServicePrice();
		calculatePayment();
		idCount++;
    }
	
	@Override
	public void calculatePayment() {
		double discount = 0;
		if (paymentMethod.equalsIgnoreCase("Saldo Coin")) {
			discount = getTotalServicePrice() * RATES_DISCOUNT_SALDO_COIN;
		}else {
			discount = getTotalServicePrice() * RATES_DISCOUNT_CASH;
		}
		
		setTotalPayment(getTotalServicePrice() - discount);
	}

	@Override
	public void calculateServicePrice() {
		double totalPrice = getServices().stream().mapToDouble(service -> service.getPrice()).sum();
		setTotalServicePrice(totalPrice);
	}

	
}
