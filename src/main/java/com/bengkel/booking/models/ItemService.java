package com.bengkel.booking.models;

import com.bengkel.booking.interfaces.IItemService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemService implements IItemService{
	private String serviceId;
	private String serviceName;
	private String vehicleType;
	private double price;
	private static int[] idCount = new int[LIST_TYPE_VAHICLE.length];

    public ItemService( String serviceName, String vehicleType, double price) {
        this.price = price;
        this.serviceName = serviceName;
        this.vehicleType = vehicleType;
		if (getVehicleType().equalsIgnoreCase(LIST_TYPE_VAHICLE[0])) {
			serviceId = "Svcm-00" + (idCount[0] +1);
			idCount[0]++;
		}
		else if (getVehicleType().equalsIgnoreCase(LIST_TYPE_VAHICLE[1])) {
			serviceId = "Svcc-00" + (idCount[1] +1);
			idCount[1]++;
		}
    }
	
}
