package com.bengkel.booking.repositories;

import java.util.Arrays;
import java.util.List;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;

public class ItemServiceRepository {
	
	public static List<ItemService> getAllItemService(){
		List<ItemService> listItemService;
		
		ItemService itemService1 = new ItemService("Ganti Oli", "Motorcyle", 70000);
		ItemService itemService2 = new ItemService("Service Mesin", "Motorcyle", 150000);
		ItemService itemService3 = new ItemService("Service CVT", "Motorcyle", 50000);
		ItemService itemService4 = new ItemService("Ganti Ban", "Motorcyle", 200000);
		ItemService itemService5 = new ItemService("Ganti Rem", "Motorcyle", 90000);
		
		ItemService itemService6 = new ItemService("Ganti Oli", "Car", 170000);
		ItemService itemService7 = new ItemService("Service Mesin", "Car", 500000);
		ItemService itemService8 = new ItemService("Service AC", "Car", 250000);
		ItemService itemService9 = new ItemService("Isi Freon AC", "Car", 70000);
		ItemService itemService10 = new ItemService("Cuci Mobil", "Car", 50000);
		
		listItemService = Arrays.asList(itemService1, itemService2, itemService3, itemService4, itemService5, itemService6, itemService7, itemService8, itemService9, itemService10);
	
		return listItemService;
	}
}
